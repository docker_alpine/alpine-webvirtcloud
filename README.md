# alpine-webvirtcloud
![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721/alpine-webvirtcloud)
![Docker Stars](https://img.shields.io/docker/stars/forumi0721/alpine-webvirtcloud)



----------------------------------------
### x64
![Docker Image Size](https://img.shields.io/docker/image-size/forumi0721/alpine-webvirtcloud/x64)



----------------------------------------
#### Description

* Distribution : [Alpine Linux](https://alpinelinux.org/)
* Architecture : x64
* Appplication : [WebVirtCloud](https://github.com/retspen/webvirtcloud)
    - WebVirtCloud is virtualization web interface for admins and users.



----------------------------------------
#### Run

```sh
docker run -d \
           -p 8000:8000/tcp \
           -p 6080:6080/tcp \
		   -e WS_PUBLIC_PORT=6080 \
		   -e WS_CERT=<ssl-certs> \
           -e SECRET_KEY=<secret_key> \
           forumi0721/alpine-webvirtcloud:[ARCH_TAG]
```



----------------------------------------
#### Usage

* URL : [http://localhost:8000/](http://localhost:8000/)
    - Default username/password : admin/admin
	- If you want to use https, you must set valid ssl-cert (WS_CERT).
	- You must open firewall WS_PUBLIC_PORT (http or https).
    - If you want to store settings, you need to add `-v /conf.d/webvirtcloud:/conf.d/webvirtcloud` to docker option.



----------------------------------------
#### Docker Options

| Option             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Ports

| Port               | Description                                      |
|--------------------|--------------------------------------------------|
| 8000/tcp           | HTTP port (Service Port)                         |
| 6080/tcp           | WebSocket port (Service Port)                    |


#### Volumes

| Volume             | Description                                      |
|--------------------|--------------------------------------------------|
| /conf.d            | Persistent data                                  |


#### Environment Variables

| ENV                | Description                                      |
|--------------------|--------------------------------------------------|
| TIMEZONE           | Timezone (default : Asia/Seoul)                  |
| SERCET_KEY         | Secret Key (default : 01234567890 * 5)           |
| WS_PUBLIC_PORT     | WS Public Port (Default : 6080)                  |
| WS_CERT            | SSL Cert file                                    |

