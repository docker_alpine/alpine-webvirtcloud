#!/bin/sh

. /app/virtualenv/bin/activate

python -c "exec(\"import random, string\nhaystack = string.ascii_letters + string.digits + string.punctuation\nprint(''.join([random.SystemRandom().choice(haystack) for _ in range(50)]))\")"

