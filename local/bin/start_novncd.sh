#!/bin/sh

. /app/virtualenv/bin/activate

WS_PORT=${WS_PORT:-6080}

cd /app/webvirtcloud
exec python3 /app/webvirtcloud/console/novncd -p ${WS_PORT}

