#!/bin/sh

. /app/virtualenv/bin/activate

cd /app/webvirtcloud
exec gunicorn webvirtcloud.wsgi:application -c /app/webvirtcloud/gunicorn.conf.py

